package org.bitbucket.properties;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesLoader {

    /*private final Properties properties = new Properties();

    private final File file = new File("D:/Alex2/devEducation/DevEducation/practice-work-dev-education/practice-work-properties/src/main/resources/application-default.properties");

    private Map<String, Object> properties = new HashMap<>();

    public String getProperty(String key){
        try {
            properties.load(new FileReader(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        return password;
    }*/

    private static Map<String, String> properties = new HashMap<>();

    public static String getProperties(String path, String key) throws IOException {
        FileReader fileReader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        while ((line = bufferedReader.readLine()) != null){
            String[] tmp = line.split("=");
            properties.put(tmp[0], tmp[1]);
        }
        if (key != null){
            return properties.get(key);
        }
        else {
            throw new RuntimeException("isNull");
        }
    }
}
