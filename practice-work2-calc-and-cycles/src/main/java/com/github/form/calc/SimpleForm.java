package com.github.form.calc;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class SimpleForm extends JFrame {
    private JTextField tfFirstNum;
    private JTextField tfOperation;
    private JTextField tfSecondNum;
    private JTextField tfResult;

    public SimpleForm(ActionManager ac) {
        super("Calculator");
        JPanel calculator = new JPanel();
        calculator.setLayout(null);

        JLabel lblFirstNum = new JLabel("Число 1");
        lblFirstNum.setBounds(30, 45, 195, 20);

        JLabel lblSecondNum = new JLabel("Число 2");
        lblSecondNum.setBounds(30, 113, 195, 20);

        JLabel lblOperation = new JLabel("Операция");
        lblOperation.setBounds(20, 183, 195, 20);

        JLabel lblResult = new JLabel("Результат");
        lblResult.setBounds(20, 333, 195, 20);


        this.tfFirstNum = new JTextField(20);
        tfFirstNum.setBounds(100, 30, 260, 50);

        this.tfSecondNum = new JTextField(20);
        tfSecondNum.setBounds(100, 100, 260, 50);

        this.tfOperation = new JTextField(20);
        tfOperation.setBounds(100, 170, 260, 50);

        this.tfResult = new JTextField(20);
        tfResult.setBounds(100, 320, 260, 50);

        //JButton btnCalc = new JButton("Посчитать");
        //btnCalc.setBounds(28, 243, 330, 60);

        ac.setTfFirstNum(this.tfFirstNum);
        ac.setTfSecondNum(this.tfSecondNum);
        ac.setTfOperation(this.tfOperation);
        ac.setTfResult(this.tfResult);

        //btnCalc.addActionListener(ac.getBtnEqActionListener());

        calculator.add(lblFirstNum);
        calculator.add(lblOperation);
        calculator.add(lblSecondNum);
        calculator.add(tfFirstNum);
        calculator.add(tfOperation);
        calculator.add(tfSecondNum);
        //calculator.add(btnCalc);

        calculator.add(lblResult);
        calculator.add(tfResult);

        setSize(400, 440);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setContentPane(calculator);
        setResizable(false);


        //Работа с цветом и бордерами
        Color color = new Color(221, 226, 206);
        Color color1 = new Color(243, 244, 238);
        calculator.setBackground(color);
        //btnCalc.setBackground(color);

        Border border = new LineBorder(Color.BLACK);
        calculator.setBorder(border);
        tfFirstNum.setBorder(border);
        tfSecondNum.setBorder(border);
        tfOperation.setBorder(border);
        tfResult.setBorder(border);
        //btnCalc.setBorder(border);

        //btnCalc.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
//-------------------------------------------------------------------------------------
        JButton button = new JButton();
        try {
            Image img = ImageIO.read(getClass().getResource("/com/github/form/button3.png"));
            button.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            System.out.println(ex);
        }
        calculator.add(button);
        button.setBounds(28, 243, 330, 60);
        button.addActionListener(ac.getBtnEqActionListener());



    }

}
