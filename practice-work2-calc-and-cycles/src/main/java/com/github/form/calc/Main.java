package com.github.form.calc;

import javax.swing.*;
import java.io.IOException;


public class Main {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Throwable thrown) {
            thrown.printStackTrace();
        }
        SimpleForm simpleForm = new SimpleForm(new ActionManager());
        simpleForm.setVisible(true);
    }
}
