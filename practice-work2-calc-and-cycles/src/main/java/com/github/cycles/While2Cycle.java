package com.github.cycles;

//While i=-20 … i=20 and for
public class While2Cycle {
    public static void main(String[] args) {
        int i = -20;
        while (i <= 20) {
            System.out.print(i + " ");
            i++;
        }
        System.out.println();
        for (int j = -20; j <= 20; j++) {
            System.out.print(j + " ");
        }
    }
}
