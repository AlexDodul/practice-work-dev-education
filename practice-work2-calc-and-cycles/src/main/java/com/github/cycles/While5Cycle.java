package com.github.cycles;

//While i=1 … i=100 кратное 3 и 5;
public class While5Cycle {
    public static void main(String[] args) {
        int i = 0;
        while (i <= 100) {
            if (i % 3 == 0) {
                System.out.print(i + " ");
            }else if (i % 5 == 0){
                System.out.print(i + " ");
            }
            i++;
        }

        System.out.println();

        for (int j = 0; j <= 100; j++) {
            if (j % 3 == 0) {
                System.out.print(j + " ");
            }else if (j % 5 == 0){
                System.out.print(j + " ");
            }
        }
    }
}
