package com.github.cycles;

//While i=0 … i=-10 and for

public class While1Cycle {
    public static void main(String[] args) {
        int i = 0;
        while (i >= -10) {
            System.out.print(i + " ");
            i--;
        }
        System.out.println();
        for (int j = 0; j >= -10; j--) {
            System.out.print(j + " ");
        }
    }
}
