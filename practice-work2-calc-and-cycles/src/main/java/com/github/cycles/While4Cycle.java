package com.github.cycles;

//While i=1 … i=100 кратное семи and for
public class While4Cycle {
    public static void main(String[] args) {
        int i = 0;
        while (i <= 100) {
            if (i % 7 == 0) {
                System.out.print(i + " ");
            }
            i++;
        }

        System.out.println();

        for (int j = 0; j <= 100; j++) {
            if (j % 7 == 0) {
                System.out.print(j + " ");
            }
        }
    }
}
