package com.github.cycles;

//While i=30 … i=10 and for
public class While3Cycle {
    public static void main(String[] args) {
        int i = 30;
        while (i >= 10) {
            System.out.print(i + " ");
            i--;
        }
        System.out.println();
        for (int j = 30; j >= 10; j--) {
            System.out.print(j + " ");
        }
    }
}
