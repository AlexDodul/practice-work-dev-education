package com.github.arrays;

import java.util.Arrays;

//Создайте массив из всех нечётных чисел от 1 до 99, выведите его на экран в строку
public class ArrayOdd {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(arr(99)));
    }
    public static int[] arr(int a){
        int count = 0;

        for (int i = 1; i <= a; i++){
            if (i % 2 != 0){
                count++;
            }
        }

        int[] array = new int[count];
        int counter = 0;
        for (int i = 1; i <= a; i++) {
            if (i % 2 != 0){
                array[counter] = i;
                counter++;
            }
        }
        return array;
    }
}
