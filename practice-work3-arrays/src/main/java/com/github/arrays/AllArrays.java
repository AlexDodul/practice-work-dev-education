package com.github.arrays;

import java.util.Arrays;

public class AllArrays {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(array15ElementRandom()));
        System.out.println(Arrays.toString(array8ElementRandom()));
        System.out.println(Arrays.toString(array4ElementRandom()));
        System.out.println(Arrays.toString(arrayFibonachi()));
        System.out.println(maxElement());
    }

    // Создайте массив из 15 случайных целых чисел из отрезка [0;9]. Выведите массив на экран.
    public static int[] array15ElementRandom() {
        int[] arr = new int[15];
        for (int i = 0; i < 15; i++) {
            int a = (int) (Math.random() * 10);
            arr[i] = a;
        }
        return arr;
    }

    // Создайте массив из 8 случайных целых чисел из отрезка [1;10]. Выведите массив на экран в строку.
    public static int[] array8ElementRandom() {
        int[] arr = new int[8];
        for (int i = 0; i < 8; i++) {
            int a = (int) (Math.random() * 10) + 1;
            arr[i] = a;
        }
        return arr;
    }

    // Создайте массив из 4 случайных целых чисел из отрезка [10;99], выведите его на экран
    public static int[] array4ElementRandom() {
        int[] arr = new int[4];
        for (int i = 0; i < 4; i++) {
            int a = (int) (Math.random() * 90) + 10;
            arr[i] = a;
        }
        return arr;
    }

    // Создайте массив из 20-ти первых чисел Фибоначчи и выведите его на экран.
    public static int[] arrayFibonachi() {
        int[] arr = new int[20];
        for (int i = 0; i < 20; i++) {
            if (i < 2) {
                arr[i] = 1;
            } else {
                arr[i] = arr[i - 1] + arr[i - 2];
            }
        }
        return arr;
    }

    // Создайте массив из 12 случайных целых чисел из отрезка [-15;15].
    // Определите какой элемент является в этом массиве максимальным вывести его.
    public static int maxElement() {
        int[] arr = new int[30];
        for (int i = 0; i < 30; i++) {
            int a = (int) (Math.random() * 30 + 1) - 15;
            arr[i] = a;
        }
        System.out.println(Arrays.toString(arr));
        int max = arr[0];
        int index = 0;
        for (int i = 1; i < arr.length; i++) {
            if (max < arr[i]){
                max = arr[i];
                index = i;
            }
        }
        return index;
    }
}
