package com.github.arrays;

import java.util.Arrays;

//Создайте массив из всех чётных чисел от 2 до 100 и выведите элементы массива на экран
public class ArrayEven {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(arr(100)));
    }
    public static int[] arr(int a){
        int count = 0;

        for (int i = 2; i <= a; i++){
            if (i % 2 == 0){
                count++;
            }
        }

        int[] array = new int[count];
        int counter = 0;
        for (int i = 2; i <= a; i++) {
            if (i % 2 == 0){
                array[counter] = i;
                counter++;
            }
        }
        return array;
    }
}
