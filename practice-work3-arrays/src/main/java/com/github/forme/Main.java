package com.github.forme;

import java.util.LinkedList;

public class Main {
    private Node root;

    private static class Node {

        int value;

        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    public String toStrings() {
        StringBuilder str = new StringBuilder();
        Node tmp = this.root;
        while (tmp != null) {
            str.append(tmp.value).append(", ");
            tmp = tmp.next;
        }
        str.deleteCharAt(str.length() - 1);
        return str.toString();
    }


    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();
        for (int i = 0; i < 10; i++) {
            linkedList.addLast(i);
        }
        System.out.println(linkedList);
        System.out.println(linkedList);
    }
}
