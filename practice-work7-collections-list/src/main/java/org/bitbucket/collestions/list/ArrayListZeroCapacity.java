package org.bitbucket.collestions.list;

public class ArrayListZeroCapacity implements IList {

    private int[] array = new int[0];

    @Override
    public void init(int[] init) {

    }

    @Override
    public void clear() {
        this.array = new int[0];
    }

    @Override
    public int size() {
        return this.array.length;
    }

    @Override
    public int[] toArray() {
        return new int[0];
    }

    @Override
    public String toString() {
        return "";
    }

    @Override
    public void addStart(int val) {

    }

    @Override
    public void addEnd(int val) {

    }

    @Override
    public void addByPos(int pos, int val) {

    }

    @Override
    public int removeStart() {
        return 0;
    }

    @Override
    public int removeEnd() {
        return 0;
    }

    @Override
    public int removeByPos(int pos) {
        return 0;
    }

    @Override
    public int max() {
        return 0;
    }

    @Override
    public int min() {
        return 0;
    }

    @Override
    public int maxPos() {
        return 0;
    }

    @Override
    public int minPos() {
        return 0;
    }

    @Override
    public int[] sort() {
        return new int[0];
    }

    @Override
    public int get(int pos) {
        return 0;
    }

    @Override
    public int[] halfRevers() {
        return new int[0];
    }

    @Override
    public int[] revers() {
        return new int[0];
    }

    @Override
    public void set(int pos, int val) {

    }
}
