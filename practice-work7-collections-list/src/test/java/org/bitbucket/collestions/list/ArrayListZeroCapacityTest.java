package org.bitbucket.collestions.list;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class ArrayListZeroCapacityTest {

    private final IList list = new ArrayListZeroCapacity();

    //=================================================
    //=================== Clear ========================
    //=================================================

    @Test
    public void clearMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearZero() {
        int[] array = {};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Size ========================
    //=================================================

    @Test
    public void sizeMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 10;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 2;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int exp = 0;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    //=================================================
    //=================== Add Start ===================
    //=================================================

    @Test
    public void addStartMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1, 1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1, 1, 232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Add End =====================
    //=================================================

    @Test
    public void addEndMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1, 232, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.addStart(2);
        int[] exp = new int[]{1, 2};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Add By Pos ==================
    //=================================================

    @Test
    public void addByPosMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.addByPos(0, 5);
        int[] exp = new int[]{5, 1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.addByPos(1, 5);
        int[] exp = new int[]{1, 5, 232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.addByPos(1, 5);
        int[] exp = new int[]{1, 5};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.addByPos(0, 5);
        int[] exp = new int[]{5};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Remove Start ================
    //=================================================

    @Test
    public void removeStartMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.removeStart();
        int[] exp = new int[]{232, 43432, 123, 543, 4343, 123, 5644, 34, 12, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeStartTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.removeStart();
        int[] exp = new int[]{232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeStartOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.removeStart();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeStartZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.removeStart();
    }

    //=================================================
    //=================== Remove End ==================
    //=================================================

    @Test
    public void removeEndMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.removeEnd();
        int[] exp = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeEndTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.removeEnd();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeEndOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.removeEnd();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeEndZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.removeEnd();
    }

    //=================================================
    //================== Remove By Pos ================
    //=================================================

    @Test
    public void removeByPosMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.removeByPos(2);
        int[] exp = new int[]{1, 232, 123, 543, 4343, 123, 5644, 34, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPosTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.removeByPos(1);
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPosOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.removeByPos(0);
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByPosZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.removeByPos(2);
    }

    //=================================================
    //=================== Max =========================
    //=================================================

    @Test
    public void maxMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.max();
        int exp = 43432;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void maxTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.max();
        int exp = 232;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void maxOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.max();
        int exp = 1;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void maxZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.max();
    }

    //=================================================
    //=================== Min =========================
    //=================================================

    @Test
    public void minMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.min();
        int exp = 1;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void minTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.min();
        int exp = 1;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void minOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.min();
        int exp = 1;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void minZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.min();
    }

    //=================================================
    //=================== Max Pos =========================
    //=================================================

    @Test
    public void maxPosMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.maxPos();
        int exp = 2;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void maxPosTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.maxPos();
        int exp = 1;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void maxPosOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.maxPos();
        int exp = 0;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void maxPosZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.maxPos();
    }

    //=================================================
    //================== Min Pos ======================
    //=================================================

    @Test
    public void minPosMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.minPos();
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void minPosTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.minPos();
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void minPosOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.minPos();
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void minPosZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.minPos();
    }

    //=================================================
    //================== Sort ======================
    //=================================================

    @Test
    public void sortMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{1, 12, 34, 123, 123, 232, 543, 4343, 5644, 43432};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{1, 232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void sortZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //===================== Get =======================
    //=================================================

    @Test
    public void getMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 43432;
        int act = this.list.get(2);
        assertEquals(exp, act);
    }

    @Test
    public void getTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 232;
        int act = this.list.get(1);
        assertEquals(exp, act);
    }

    @Test
    public void getOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.get(0);
        assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void getZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int act = this.list.get(2);
    }

    //=================================================
    //================== Half Revers ==================
    //=================================================

    @Test
    public void halfReversMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.halfRevers();
        int[] exp = new int[]{232, 543, 4343, 5644, 43432, 1, 12, 34, 123, 123};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.halfRevers();
        int[] exp = new int[]{232, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.halfRevers();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void halfReversZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.halfRevers();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //==================== Revers =====================
    //=================================================

    @Test
    public void reversMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.revers();
        int[] exp = new int[]{12, 34, 5644, 123, 4343, 543, 123, 43432, 232, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void reversTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.revers();
        int[] exp = new int[]{232, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void reversOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.revers();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void reversZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.revers();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //==================== Revers =====================
    //=================================================

    @Test
    public void setMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.set(2, 15);
        int[] exp = new int[]{11, 232, 15, 123, 543, 4343, 123, 5644, 34, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.set(1, 15);
        int[] exp = new int[]{1, 15};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.set(0, 15);
        int[] exp = new int[]{15};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void setZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.set(0, 15);
    }
}