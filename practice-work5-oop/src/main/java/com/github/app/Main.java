package com.github.app;

import java.util.Arrays;
import java.util.List;

import static com.github.app.CustomerArray.customers;
import static com.github.app.StudentArray.students;

public class Main {
    public static void main(String[] args) {
        System.out.println("============================== Students ==============================");
        System.out.println(Arrays.toString(new List[]{students}));
        System.out.println(new StudentDao().faculty(students, 1));
        System.out.println(new StudentDao().facultyAndCourse(students, 2, 2));
        System.out.println(new StudentDao().group(students, "430-m"));
        System.out.println(new StudentDao().birth(students, 1992));
        System.out.println();
        System.out.println("============================== Customers ==============================");
        System.out.println(Arrays.toString(new List[]{customers}));
        System.out.println(new CustomerDao().name(customers, "Alex"));
        System.out.println(new CustomerDao().cardNumber(customers, "0000-0000-0000-0003"));
    }
}
