package com.github.app;

import java.util.ArrayList;
import java.util.List;

public class CustomerArray {
    public static List<Customer> customers = new ArrayList<>();

    static {
        customers.add(new Customer(1L, "Dodul", "Alex", "Alex", "Odessa", "0000-0000-0000-0001", "0000-0000-0000-0002"));
        customers.add(new Customer(2L, "Krasov", "Anton", "Kraz", "Kiev", "0000-0000-0000-0003", "0000-0000-0000-0004"));
        customers.add(new Customer(3L, "Repa", "Sergey", "Seryi", "Kiev", "0000-0000-0000-0005", "0000-0000-0000-0006"));
        customers.add(new Customer(4L, "Zerg", "Vasya", "Zerg", "Dnepr", "0000-0000-0000-0007", "0000-0000-0000-0008"));
        customers.add(new Customer(5L, "Kora", "Anastasiya", "Kora", "Kharkov", "0000-0000-0000-0009", "0000-0000-0000-0010"));
        customers.add(new Customer(6L, "Beliy", "Anton", "Beliy", "Kharkov", "0000-0000-0000-00011", "0000-0000-0000-0012"));
    }
}
