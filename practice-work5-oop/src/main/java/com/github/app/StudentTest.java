package com.github.app;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.github.app.StudentArray.students;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class StudentTest {

    @Test
    public void faculty() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(2L, "Krasov", "Anton", "Kraz", 1990, "Kiev", "0500000001", 2, 2, "421"));
        exp.add(new Student(3L, "Repa", "Sergey", "Seryi", 1992, "Odessa", "0670000002", 2, 2, "425"));
        List<Student> act = new StudentDao().faculty(students, 2);
        assertEquals(exp, act);
    }

    @Test
    public void facultyNull() {
        List<Student> act = new StudentDao().faculty(null, 2);
        assertNull(act);
    }

    @Test
    public void facultyEmpty() {
        List<Student> act = new StudentDao().faculty(new ArrayList<>(), 2);
        assertNull(act);
    }

    @Test
    public void facultyOddNull() {
        List<Student> act = new StudentDao().faculty(students, -1);
        assertNull(act);
    }

    @Test
    public void facultyAndCourse() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(2L, "Krasov", "Anton", "Kraz", 1990, "Kiev", "0500000001", 2, 2, "421"));
        exp.add(new Student(3L, "Repa", "Sergey", "Seryi", 1992, "Odessa", "0670000002", 2, 2, "425"));
        List<Student> act = new StudentDao().facultyAndCourse(students, 2, 2);
        assertEquals(exp, act);
    }

    @Test
    public void facultyAndCourseArrNull() {
        List<Student> act = new StudentDao().facultyAndCourse(null, 2, 2);
        assertNull(act);
    }

    @Test
    public void facultyAndCourseEmpty() {
        List<Student> act = new StudentDao().facultyAndCourse(new ArrayList<>(), 2, 2);
        assertNull(act);
    }

    @Test
    public void facultyAndCourseFacOdd() {
        List<Student> act = new StudentDao().facultyAndCourse(students, -1, 2);
        assertNull(act);
    }

    @Test
    public void facultyAndCourseCourseOdd() {
        List<Student> act = new StudentDao().facultyAndCourse(students, 2, -2);
        assertNull(act);
    }

    @Test
    public void group() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1L, "Dodul", "Alex", "Alex", 1991, "Kiev", "0500000000", 1, 3, "430-m"));
        exp.add(new Student(5L, "Kora", "Anastasiya", "Kora", 1999, "Odessa", "0670000005", 1, 3, "430-m"));
        List<Student> act = new StudentDao().group(students, "430-m");
        assertEquals(exp, act);
    }

    @Test
    public void groupArrNull() {
        List<Student> act = new StudentDao().group(null, "430-m");
        assertNull(act);
    }

    @Test
    public void groupArrEmpty() {
        List<Student> act = new StudentDao().group(new ArrayList<>(), "430-m");
        assertNull(act);
    }

    @Test
    public void groupNull() {
        List<Student> act = new StudentDao().group(students, null);
        assertNull(act);
    }

    @Test
    public void groupEmpty() {
        List<Student> act = new StudentDao().group(students, "");
        assertNull(act);
    }

    @Test
    public void birthOne() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(4L, "Zerg", "Vasya", "Zerg", 2000, "Kiev", "0500000003", 1, 1, "410-m"));
        List<Student> act = new StudentDao().birth(students, 1999);
        assertEquals(exp, act);
    }

    @Test
    public void birthTwo() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(4L, "Zerg", "Vasya", "Zerg", 2000, "Kiev", "0500000003", 1, 1, "410-m"));
        exp.add(new Student(5L, "Kora", "Anastasiya", "Kora", 1999, "Odessa", "0670000005", 1, 3, "430-m"));
        List<Student> act = new StudentDao().birth(students, 1995);
        assertEquals(exp, act);
    }

    @Test
    public void birthArrNull() {
        List<Student> act = new StudentDao().birth(null, 1995);
        assertNull(act);
    }

    @Test
    public void birthArrEmpty() {
        List<Student> act = new StudentDao().birth(new ArrayList<>(), 1995);
        assertNull(act);
    }

    @Test
    public void birthOdd() {
        List<Student> act = new StudentDao().birth(students, -1990);
        assertNull(act);
    }
}
