package com.github.app;

import java.util.ArrayList;
import java.util.List;

public class StudentArray {

    public static List<Student> students = new ArrayList<>();

    static {
        students.add(new Student(1L, "Dodul", "Alex", "Alex", 1991, "Kiev", "0500000000", 1, 3, "430-m"));
        students.add(new Student(2L, "Krasov", "Anton", "Kraz", 1990, "Kiev", "0500000001", 2, 2, "421"));
        students.add(new Student(3L, "Repa", "Sergey", "Seryi", 1992, "Odessa", "0670000002", 2, 2, "425"));
        students.add(new Student(4L, "Zerg", "Vasya", "Zerg", 2000, "Kiev", "0500000003", 1, 1, "410-m"));
        students.add(new Student(5L, "Kora", "Anastasiya", "Kora", 1999, "Odessa", "0670000005", 1, 3, "430-m"));
    }
}
