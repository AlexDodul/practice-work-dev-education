package com.github.app;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static com.github.app.CustomerArray.customers;

public class CustomerTest {

    @Test
    public void nameOne() {
        List<Customer> exp = new ArrayList<>();
        exp.add(new Customer(1L, "Dodul", "Alex", "Alex", "Odessa", "0000-0000-0000-0001", "0000-0000-0000-0002"));
        List<Customer> act = new CustomerDao().name(customers, "Alex");
        assertEquals(exp, act);
    }

    @Test
    public void nameTwo() {
        List<Customer> exp = new ArrayList<>();
        exp.add(new Customer(2L, "Krasov", "Anton", "Kraz", "Kiev", "0000-0000-0000-0003", "0000-0000-0000-0004"));
        exp.add(new Customer(6L, "Beliy", "Anton", "Beliy", "Kharkov", "0000-0000-0000-00011", "0000-0000-0000-0012"));
        List<Customer> act = new CustomerDao().name(customers, "Anton");
        assertEquals(exp, act);
    }

    @Test
    public void nameNull() {
        List<Customer> act = new CustomerDao().name(null, "Anton");
        assertNull(act);
    }

    @Test
    public void nameEmpty() {
        List<Customer> act = new CustomerDao().name(new ArrayList<>(), "Anton");
        assertNull(act);
    }

    @Test
    public void nameStrNull() {
        List<Customer> act = new CustomerDao().name(customers, null);
        assertNull(act);
    }

    @Test
    public void nameStrEmpty() {
        List<Customer> act = new CustomerDao().name(customers, "");
        assertNull(act);
    }

    @Test
    public void cardNumberOne() {
        List<Customer> exp = new ArrayList<>();
        exp.add(new Customer(1L, "Dodul", "Alex", "Alex", "Odessa", "0000-0000-0000-0001", "0000-0000-0000-0002"));
        List<Customer> act = new CustomerDao().cardNumber(customers, "0000-0000-0000-0001");
        assertEquals(exp, act);
    }

    @Test
    public void cardNumberNull() {
        List<Customer> act = new CustomerDao().cardNumber(null, "0000-0000-0000-0001");
        assertNull(act);
    }

    @Test
    public void cardNumberEmpty() {
        List<Customer> act = new CustomerDao().cardNumber(new ArrayList<>(), "0000-0000-0000-0001");
        assertNull(act);
    }

    @Test
    public void cardNumberStrNull() {
        List<Customer> act = new CustomerDao().cardNumber(customers, null);
        assertNull(act);
    }

    @Test
    public void cardNumberStrEmpty() {
        List<Customer> act = new CustomerDao().cardNumber(customers, "");
        assertNull(act);
    }
}
