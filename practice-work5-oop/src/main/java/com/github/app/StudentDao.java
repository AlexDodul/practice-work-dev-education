package com.github.app;

import java.util.ArrayList;
import java.util.List;

public class StudentDao {
    public List<Student> faculty(List<Student> students, int fac) {
        if (students == null || students.isEmpty()) {
            return null;
        }
        if (fac <= 0) {
            return null;
        }
        List<Student> result = new ArrayList<>();
        for (Student student : students) {
            if (student.getFaculty() == fac) {
                result.add(student);
            }
        }
        return result;
    }

    public List<Student> facultyAndCourse(List<Student> students, int fac, int course) {
        if (students == null || students.isEmpty()) {
            return null;
        }
        if (fac <= 0) {
            return null;
        }
        if (course <= 0) {
            return null;
        }
        List<Student> result = new ArrayList<>();
        for (Student student : students) {
            if (student.getFaculty() == fac && student.getCourse() == course) {
                result.add(student);
            }
        }
        return result;
    }

    public List<Student> group(List<Student> students, String group) {
        if (students == null || students.isEmpty()) {
            return null;
        }
        if (group == null || group.isEmpty()) {
            return null;
        }
        List<Student> result = new ArrayList<>();
        for (Student student : students) {
            if (student.getGroup().equals(group)) {
                result.add(student);
            }
        }
        return result;
    }

    public List<Student> birth(List<Student> students, int birth) {
        if (students == null || students.isEmpty()) {
            return null;
        }
        if (birth <= 0) {
            return null;
        }
        List<Student> result = new ArrayList<>();
        for (Student student : students) {
            if (student.getDataOfBirth() > birth) {
                result.add(student);
            }
        }
        return result;
    }
}
