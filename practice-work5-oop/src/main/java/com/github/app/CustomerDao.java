package com.github.app;

import java.util.ArrayList;
import java.util.List;

public class CustomerDao {
    public List<Customer> name(List<Customer> customers, String name) {
        if (customers == null || customers.isEmpty()) {
            return null;
        }
        if (name == null || name.isEmpty()) {
            return null;
        }
        List<Customer> result = new ArrayList<>();
        for (Customer customer : customers) {
            if (customer.getName().equals(name)) {
                result.add(customer);
            }
        }
        return result;
    }

    public List<Customer> cardNumber(List<Customer> customers, String card) {
        if (customers == null || customers.isEmpty()) {
            return null;
        }
        if (card == null || card.isEmpty()) {
            return null;
        }
        List<Customer> result = new ArrayList<>();
        for (Customer customer : customers) {
            if (customer.getCreditCardNumber().equals(card)) {
                result.add(customer);
            }
        }
        return result;
    }
}
