package org.bitbucket.springexample.firstapp.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/firstApp")
public class Controller {

    @GetMapping("/get/{text}")
    public String getHello(@PathVariable String text){
        return "<h1>" + text + "</h1>";
    }

}
