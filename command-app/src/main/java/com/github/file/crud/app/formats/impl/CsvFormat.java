package com.github.file.crud.app.formats.impl;

import com.github.file.crud.app.formats.BaseFormat;
import com.github.file.crud.app.models.Person;

import java.util.List;

public class CsvFormat implements BaseFormat {
    @Override
    public String toFormat(Person person) {
        if (person == null) {
            return null;
        } else {
            return String.format(
                    "%d,%s,%s,%d,%s\n",
                    person.getId(),
                    person.getFirstName(),
                    person.getLastName(),
                    person.getAge(),
                    person.getCity()
            );
        }
    }

    @Override
    public Person fromFormat(String str) {
        if (str == null || "".equals(str)){
            return null;
        }
        str = str.replace("\n", "");
        String[] array = str.split(",");
        return new Person(
                Long.parseLong(array[0]),
                array[1],
                array[2],
                Integer.parseInt(array[3]),
                array[4]
        );
    }

    @Override
    public String toFormatArray(List<Person> people) {
        if (people == null || people.isEmpty()){
            return null;
        }
        StringBuilder str = new StringBuilder();
        for (Person p : people){
            str.append(toFormat(p));
        }
        return str.toString();
    }
}
