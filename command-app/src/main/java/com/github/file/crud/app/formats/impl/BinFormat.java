package com.github.file.crud.app.formats.impl;

import com.github.file.crud.app.models.Person;
import com.github.file.crud.app.utils.Paths;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BinFormat {
    public static byte[] scanFile(String fileName) {
        String builder = Paths.getBinBasePath() +
                fileName;
        File file = new File(builder);
        FileInputStream fis;
        byte[] data = new byte[(int) file.length()];
        try{
            fis = new FileInputStream(file);
            fis.read(data);
            fis.close();

            return data;

        } catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

    public static void printFile(byte[] data, String fileName) throws IOException {
        String pathToFile = Paths.getBinBasePath() + fileName;
        File file = new File(pathToFile);
        if(!file.exists()){
            file.createNewFile();
        }

        FileOutputStream fos;
        try{
            fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();

        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static List<Person> fromFormat(byte[] file){
        if(file == null || Arrays.equals(file, new byte[0])){
            return null;
        }

        List<Person> people = new ArrayList<>();

        try(ByteArrayInputStream bais = new ByteArrayInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(bais)) {
            people = (List<Person>) ois.readObject();
        }
        catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return people;
    }

    public static byte[] toFormat(List<Person> people){
        byte[] stream = null;

        try(ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos)){
            oos.writeObject(people);
            stream = baos.toByteArray();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return stream;
    }
}
