package com.github.file.crud.app.formats.impl;


import com.github.file.crud.app.formats.BaseFormat;
import com.github.file.crud.app.models.Person;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CsvFormatTest {

    private BaseFormat format = new CsvFormat();

    @Test
    public void toFormat(){
        String exp = "1,Vasya,Pupkin,33,Odessa\n";
        Person data = new Person(1, "Vasya", "Pupkin", 33, "Odessa");
        String act = this.format.toFormat(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void toFormatNull(){
        String act = this.format.toFormat(null);
        Assert.assertNull(act);
    }

    @Test
    public void formFormat(){
        String data = "1,Vasya,Pupkin,33,Odessa\n";
        Person exp = new Person(1L, "Vasya", "Pupkin", 33, "Odessa");
        Person act = this.format.fromFormat(data);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void formFormatNull(){
        Person act = this.format.fromFormat(null);
        Assert.assertNull(act);
    }

    @Test
    public void formFormatEmpty(){
        Person act = this.format.fromFormat("");
        Assert.assertNull(act);
    }

    @Test
    public void toFormatArray(){
        String exp = "1,Vasya,Pupkin,33,Odessa\n" + "2,Vasya,Pupkin,33,Odessa\n"
                + "3,Vasya,Pupkin,33,Odessa\n" + "4,Vasya,Pupkin,33,Odessa\n"
                + "5,Vasya,Pupkin,33,Odessa\n";
        List<Person> data = new ArrayList<>();
        data.add(new Person(1L, "Vasya", "Pupkin", 33, "Odessa"));
        data.add(new Person(2L, "Vasya", "Pupkin", 33, "Odessa"));
        data.add(new Person(3L, "Vasya", "Pupkin", 33, "Odessa"));
        data.add(new Person(4L, "Vasya", "Pupkin", 33, "Odessa"));
        data.add(new Person(5L, "Vasya", "Pupkin", 33, "Odessa"));
        String act = this.format.toFormatArray(data);
        Assert.assertEquals(exp, act);
        //System.out.println(act);
    }
    @Test
    public void toFormatArrayNull(){
        String act = this.format.toFormatArray(null);
        Assert.assertNull(act);
    }
    @Test
    public void toFormatArrayEmpty(){
        String act = this.format.toFormatArray(new ArrayList<>());
        Assert.assertNull(act);
    }
}
