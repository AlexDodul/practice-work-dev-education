package com.github.file.crud.app;

import com.github.file.crud.app.models.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //Person person = new Person(1, "Vasya", "Pupkin", 33, "Kharkov");
        //System.out.println("Json Person: " + toJsonFormat(person) + "\n");
        //System.out.println("Csv Person: \n" + toCsvFormat(person) + "\n");
        //System.out.println("XML Person: \n" + toXmlFormat(person) + "\n");

        System.out.println(fromStringDataToObjects("Person: " + "\n" +
                "\tid: " + "1" + "\n" + "\tfirstName: " + "Alex" + "\n" + "\tlastName: " + "Ddl" + "\n" +
                "\tage: " + "22" + "\n" + "\tcity: " + "Odessa" + "\n"));

    }

    public static String toJsonFormat(Person person){
        return "\n{\n\"id\": " + person.getId() + "\n\"firstname\": " + person.getFirstName()
                + "\n\"lastname\": " + person.getLastName()
                + "\n\"age\": " + person.getAge()
                + "\n\"city\": " + person.getCity() + "\n}";
    }

    public static String toCsvFormat(Person person){
        return person.getId()
                + "," + person.getFirstName()
                + "," + person.getLastName()
                + "," + person.getAge()
                + "," + person.getCity();
    }

    public static String toXmlFormat(Person person){
        return "<person>"
                + "\n<id>" + person.getId() + "</id>"
                + "\n<firstName>" + person.getFirstName() + "</firstName>"
                + "\n<lastName>" + person.getLastName() + "</lastName>"
                + "\n<age>" + person.getAge() + "</age>"
                + "\n<city>" + person.getCity() + "</city>"
                + "\n</person>";
    }
//=========================================================================================
    public static List<Person> fromStringDataToObjects(String file) {

        List<Person> people = new ArrayList<>();

        long id = 0;
        String firstName = "";
        String lastName = "";
        int age = 0;
        String city = "";

        String[] arrayStr;
        String[] insideArrayStr;

        file = file.replaceAll(" ", "");
        file = file.replaceAll("\t", "");
        System.out.println(file);

        arrayStr = file.split("\n");
        System.out.println(Arrays.toString(arrayStr));

        for (int i = 0; i < arrayStr.length; i++) {
            insideArrayStr = arrayStr[i].split(":");
            int parserCode = (i);
            switch (parserCode) {
                case (0):
                    break;
                case (1):
                    id = Long.parseLong(insideArrayStr[1]);
                    break;
                case (2):
                    firstName = insideArrayStr[1];
                    break;
                case (3):
                    lastName = insideArrayStr[1];
                    break;
                case (4):
                    age = Integer.parseInt(insideArrayStr[1]);
                    break;
                case (5):
                    city = insideArrayStr[1];
                    break;
                default:
                    break;
            }
        }
        Person person = new Person(id, firstName, lastName, age, city);
        people.add(person);
        return people;
    }

}
