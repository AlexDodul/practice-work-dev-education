package com.github.file.crud.app.utils;

import java.io.File;

public class Paths {
    private static final String personBasePath = "src" + File.separator + "com" + File.separator + "github"
            + File.separator + "crud" + File.separator + "personBase";
    private static final String binBasePath =  personBasePath.concat(File.separator + "bin" + File.separator);
    private static final String csvBasePath =  personBasePath.concat(File.separator + "csv" + File.separator);
    private static final String jsonBasePath =  personBasePath.concat(File.separator + "json" + File.separator);
    private static final String xmlBasePath =  personBasePath.concat(File.separator + "xml" + File.separator);
    private static final String ymlBasePath =  personBasePath.concat(File.separator + "yml" + File.separator);

    public static String getBinBasePath() {
        return binBasePath;
    }

    public static String getCsvBasePath() {
        return csvBasePath;
    }

    public static String getJsonBasePath() {
        return jsonBasePath;
    }

    public static String getXmlBasePath() {
        return xmlBasePath;
    }

    public static String getYmlBasePath() {
        return ymlBasePath;
    }
}
