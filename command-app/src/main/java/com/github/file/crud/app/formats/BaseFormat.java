package com.github.file.crud.app.formats;

import com.github.file.crud.app.models.Person;

import java.util.List;

public interface BaseFormat {
    String toFormat(Person person);

    Person fromFormat(String str);

    String toFormatArray(List<Person> people);
}
