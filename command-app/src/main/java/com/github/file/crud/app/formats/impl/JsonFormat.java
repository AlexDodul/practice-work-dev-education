package com.github.file.crud.app.formats.impl;

import com.github.file.crud.app.formats.BaseFormat;
import com.github.file.crud.app.models.Person;

import java.util.ArrayList;
import java.util.List;

public class JsonFormat implements BaseFormat {

    @Override
    public String toFormat(Person person) {
        return null;
    }

    @Override
    public Person fromFormat(String str) {
        return null;
    }

    @Override
    public String toFormatArray(List<Person> people) {
        return null;
    }
}
