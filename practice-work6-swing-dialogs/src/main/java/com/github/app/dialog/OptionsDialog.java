package com.github.app.dialog;

import javax.swing.*;
import java.awt.*;

public class OptionsDialog {
    /*public static void main(String[] args) {
        JOptionPane.showMessageDialog(new JFrame(), "Hello World",
                "Dialog",
                JOptionPane.INFORMATION_MESSAGE);

        JOptionPane.showConfirmDialog(new JFrame(),  "Buy an elephant" , "Dialod",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        JColorChooser.showDialog(new JFrame(), "Цвет", Color.red);

        new JFileChooser().showDialog(null, "Открыть файл");
    }*/

    public static void simpleMessage(){
        JOptionPane.showMessageDialog(new JFrame(), "Simple message.");
    }

    public static void warningMessage(){
        JOptionPane.showMessageDialog(new JFrame(), "Warning message.",
                "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public static void errorMessage(JFrame frame){
        JOptionPane.showMessageDialog(frame, "Error message.",
                "Warning", JOptionPane.ERROR_MESSAGE);
    }

    public static void planeMessage(){
        JOptionPane.showMessageDialog(new JFrame(), "Plain message.",
                "Plain", JOptionPane.PLAIN_MESSAGE);
    }

    public static void infoMessage(){
        JOptionPane.showMessageDialog(new JFrame(),
                "Information message.",
                "Info",
                JOptionPane.INFORMATION_MESSAGE,
                new ImageIcon());
    }

    public static void yesNoCancelOptionDialog(){
        Object[] options = {"Yes option", "No option", "Cancel option"};
        JOptionPane.showOptionDialog(new JFrame(), "Option dialog",
                "Options", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
    }

    public static void yesNoOptionDialog(){
        Object[] options = {"Yes option", "No option"};
        JOptionPane.showOptionDialog(new JFrame(), "Option dialog",
                "Options", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
    }

    public static String comboDialog(){
        Object[] options = {"First option", "Second option", "Third option"};
        return (String) JOptionPane.showInputDialog(new JFrame(), "Combo dialog",
                "Combo", JOptionPane.PLAIN_MESSAGE, null, options, "First option");
    }

    public static String inputDialog(){
        return (String) JOptionPane.showInputDialog(new JFrame(), "Input dialog",
                "Input", JOptionPane.PLAIN_MESSAGE, null, null, "");
    }

    public static Color colorDialog(JFrame frame){
        return JColorChooser.showDialog(frame, "Select a color", Color.RED);
    }

    public static int chooseFile(){
        JFileChooser fileopen = new JFileChooser();
        return fileopen.showDialog(null, "Открыть файл");
    }

    public static int saveFile(){
        JFileChooser fileSave = new JFileChooser();
        return fileSave.showSaveDialog(null);
    }
}
