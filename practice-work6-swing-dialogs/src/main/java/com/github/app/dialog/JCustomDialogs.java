package com.github.app.dialog;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;

public class JCustomDialogs extends JFrame{
    public JCustomDialogs() throws HeadlessException {
        setSize(400, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JButton btnErrorMessage = new JButton("Error message");
        JButton btnChooseColor = new JButton("Color");
        JButton btnFileChooser = new JButton("File Chooser");
        JButton btnFileSave = new JButton("File Save");
        JButton btnMyCustom = new JButton("My Custom");

        btnErrorMessage.setBounds(10, 10, 120, 30);
        btnChooseColor.setBounds(10, 50, 120, 30);
        btnFileChooser.setBounds(10, 100, 120, 30);
        btnFileSave.setBounds(10, 150, 120, 30);
        btnMyCustom.setBounds(10, 200, 120, 30);

        btnErrorMessage.addActionListener(e -> OptionsDialog.errorMessage(this));
        btnChooseColor.addActionListener(e -> {
            Color color = OptionsDialog.colorDialog(this);
            System.out.println(color);
        });
        btnFileChooser.addActionListener(e -> {
            /*JFileChooser f = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            int r = f.showOpenDialog(null);*/
            OptionsDialog.chooseFile();
        });
        btnFileSave.addActionListener(e -> {
            /*JFileChooser f = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            int r = f.showSaveDialog(null);*/
            OptionsDialog.saveFile();
        });
        btnMyCustom.addActionListener(e -> {
            /*JFileChooser f = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            int r = f.showSaveDialog(null);*/
            JMyCustomDialog myCustomDialog = new JMyCustomDialog();
            myCustomDialog.setVisible(true);
        });

        add(btnMyCustom);
        add(btnFileSave);
        add(btnFileChooser);
        add(btnChooseColor);
        add(btnErrorMessage);

        setLayout(null);
        setVisible(Boolean.TRUE);
    }
}
