package com.github.app.dialog;

import javax.swing.*;
import java.awt.*;

public class JMyCustomDialog extends JDialog {
    static JFrame jFrame = getFrame();

    public JMyCustomDialog(){
        super(jFrame, "MyCustomDialog", true);
        add(new JTextField(), BorderLayout.NORTH);
        add(new JButton("MyCustomButton"), BorderLayout.SOUTH);
        setBounds(500, 500, 250, 150);
    }

    static JFrame getFrame(){
        JFrame jFrame = new JFrame();
        jFrame.setVisible(true);
        jFrame.setBounds(400, 400, 500,400);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        return jFrame;
    }
}
