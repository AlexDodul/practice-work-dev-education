import org.junit.Assert;
import org.junit.Test;

public class RegExpTest {

    ////Расставить пробелы после всех знаков препинания (, . : ? !)
    @Test
    public void space() {
        String exp = "Adafa afaas aafas? dsgsd. sgsdg? sgsgsg";
        String act = RegExp.space("Adafa     afaas aafas?  dsgsd.sgsdg?sgsgsg");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void spaceLittle() {
        String exp = "Hello Java! Hello Java!";
        String act = RegExp.space("Hello  Java!Hello  Java!");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void spaceMany() {
        String exp = "Asgkfgdfg, dfgkdfohdh. dfdfhtrhr rtyrtyrt, trytrterer. erterye! fdhdfhd? fdfhdfhdfd, dfdfhdfhdh.";
        String act = RegExp.space(" Asgkfgdfg,dfgkdfohdh.dfdfhtrhr rtyrtyrt,trytrterer.erterye!fdhdfhd?fdfhdfhdfd,dfdfhdfhdh. ");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void spaceNull() {
        String act = RegExp.space("");
        Assert.assertNull(act);
    }

    //Все символы строки (пути к файлу) c / перевернуть \\
    @Test
    public void slash() {
        String exp = "c:\\\\package1\\\\package2\\\\package3\\\\file.exe";
        String act = RegExp.slash("c:/package1/package2/package3/file.exe");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void slashLittle() {
        String exp = "c:\\\\file.exe";
        String act = RegExp.slash("c:/file.exe");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void slashMany() {
        String exp = "c:\\\\package1\\\\package2\\\\package3\\\\package4\\\\package5\\\\package6\\\\package7\\\\package8\\\\file.exe";
        String act = RegExp.slash("c:/package1/package2/package3/package4/package5/package6/package7/package8/file.exe");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void slashNull() {
        String act = RegExp.slash("");
        Assert.assertNull(act);
    }

    //Указать путь к файлу и провалидировать его (Pattern, Matcher) (на недопустимые символы)
    @Test
    public void valid() {
        boolean act = RegExp.valid("D:\\Users\\Alex\\Desktop\\CRUD");
        Assert.assertEquals(true, act);
    }

    @Test
    public void validLittle() {
        boolean act = RegExp.valid("D:\\Users\\");
        Assert.assertEquals(true, act);
    }

    @Test
    public void validMany() {
        boolean act = RegExp.valid("D:\\Users\\Alex\\Desktop\\CRUD\\package1\\package2\\package1\\package3\\package4");
        Assert.assertEquals(true, act);
    }

    @Test
    public void validNull() {
        boolean act = RegExp.valid("");
        Assert.assertEquals(false, act);
    }
}
