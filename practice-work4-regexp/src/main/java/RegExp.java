import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExp {
    public static void main(String[] args) {
        System.out.println(space("Adafa     afaas aafas?  dsgsd.sgsdg?sgsgsg!"));
        System.out.println(slash("c:/package1/package2/package3/file.exe"));
        System.out.println(valid("D:\\Users\\Alex\\Desktop\\CRUD"));
        System.out.println(disk("D:\\"));
        System.out.println(path("//"));
        System.out.println(fileName("D:\\Users\\Alex\\Desktop\\CRUD\\file.exe"));
        System.out.println(pathToFile("D:\\Users\\Alex\\Desktop\\CRUD\\file.exe"));

    }

    //Расставить пробелы после всех знаков препинания (, . : ? !)
    public static String space(String str) {
        if (str == null || str.equals("")){
            return null;
        }
        return str.trim().replaceAll("\\s+", " ").replaceAll("(?<=\\p{Punct})(?=\\w)", " ");
    }

    //Все символы строки (пути к файлу) c / перевернуть \\
    public static String slash(String string) {
        if (string == null || string.equals("")){
            return null;
        }
        return string.replaceAll("/", "\\\\\\\\");
    }

    //Указать путь к файлу и провалидировать его (Pattern, Matcher) (на недопустимые символы)
    public static boolean valid(String str) {
        if (str == null || str.equals("")){
            return false;
        }
        Pattern pattern = Pattern.compile("^([a-zA-Z]:)?(\\\\[a-zA-Z0-9_\\-]+)+\\\\?");
        Matcher matcher = pattern.matcher(str);
        return matcher.find();
    }

    //Проверить, что указанный путь является диском
    public static boolean disk (String str) {
        Pattern pattern = Pattern.compile("^([a-zA-Z]:\\\\)");
        Matcher matcher = pattern.matcher(str);
        return matcher.find();
    }

    //Проверить что это путь к файлу т.е путь должен заканчиваться // или \ -----------------
    public static boolean path (String str) {
        Pattern pattern = Pattern.compile("(\\/\\/)?$");
        Matcher matcher = pattern.matcher(str);
        //System.out.println(pattern);
        return matcher.find();
    }

    //Достать имя файла из пути
    public static String fileName (String str) {
        int idx = str.replaceAll("\\\\", "/").lastIndexOf("/");
        return idx >= 0 ? str.substring(idx + 1) : str;
    }

    //Проверить имя файла на допустимые символы.


    //Выбрать все содержимое пути от диска к файлу.
    public static String pathToFile (String str) {
        String s = "";
        Pattern pattern = Pattern.compile(".*[\\\\]");
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            s += str.substring(matcher.start(), matcher.end());
        }
        return s;
    }
}
